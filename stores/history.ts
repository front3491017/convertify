import { defineStore } from 'pinia';
import { v4 } from 'uuid';

interface Conversion {
  id: string;
  date: string;
  name: string;
  baseExtension: string;
  exportExtension: string;
  location: { lat: number | null; lng: number | null };
  blob: Blob;
}

export const useHistoryStore = defineStore('history', {
  state: () => ({
    conversions: [] as Conversion[]
  }),
  actions: {
    addConversion(conversion: Omit<Conversion, 'id'>) {
      this.conversions.push({
        ...conversion,
        id: v4()
      });
    },
    removeConversion(id: string) {
      this.conversions = this.conversions.filter((c) => c.id !== id);
    }
  },
  persist: {
    key: 'history',
    storage: localStorage
  }
});
