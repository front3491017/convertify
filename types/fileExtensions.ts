export enum DocumentExtension {
  DOCX = 'docx',
  PDF = 'pdf',
  XLSX = 'xlsx',
  PPTX = 'pptx',
  ODT = 'odt',
  ODS = 'ods',
  ODP = 'odp',
  TXT = 'txt'
}

export enum MediaExtension {
  MP4 = 'mp4',
  AVI = 'avi',
  MKV = 'mkv',
  MOV = 'mov',
  MP3 = 'mp3',
  WAV = 'wav'
}

export enum ImageExtension {
  JPG = 'jpg',
  PNG = 'png',
  WEBP = 'webp',
  GIF = 'gif'
}

export type FilesExtension = `${DocumentExtension}` | `${MediaExtension}` | `${ImageExtension}`;
