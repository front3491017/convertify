import * as dotenv from 'dotenv';

dotenv.config();

export default defineNuxtConfig({
  // Runtime configuration
  runtimeConfig: {
    public: {
      apiUrl: process.env.API_URL
    }
  },

  // Modules
  modules: ['@pinia/nuxt', '@pinia-plugin-persistedstate/nuxt'],

  // Build configuration
  build: { transpile: ['vuetify'] },

  // CSS files to be included
  css: [
    'vue3-toastify/dist/index.css',
    '~/assets/scss/_reset.scss',
    '~/assets/scss/_variables.scss',
    '@mdi/font/css/materialdesignicons.min.css'
  ],

  // Directory aliases
  dir: {
    pages: 'UI/pages',
    layouts: 'UI/layouts'
  },

  // Route rules
  routeRules: {
    '/': { redirect: '/dashboard' }
  },

  // Disable server-side rendering
  ssr: false,

  // Plugins
  plugins: ['~/plugins/vuetify.ts', '~/plugins/toastify.ts'],

  // Auto-import configuration
  imports: {
    dirs: ['types/*.ts', 'types/**/*.ts']
  },

  // Components configuration
  components: [
    {
      path: '~/UI/components',
      pathPrefix: false
    }
  ],

  // Enable devtools
  devtools: { enabled: true }
});
