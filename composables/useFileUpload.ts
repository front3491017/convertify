import { ref, computed } from 'vue';
import { DocumentExtension, MediaExtension, ImageExtension, type FilesExtension } from '@/types/fileExtensions';
import { useGeolocation } from '@vueuse/core';
import { toast } from 'vue3-toastify';

export function useFileUpload() {
  const runtimeConfig = useRuntimeConfig();

  const historyStore = useHistoryStore();
  const { coords, isSupported } = useGeolocation();

  const uploadedFile = ref<File | null>(null);
  const convertedFile = ref<Blob | null>(null);
  const selectedExtension = ref<FilesExtension | ''>('');

  const availableExtensions = computed(() => {
    const fileExtension = uploadedFile.value?.name.split('.').pop();
    switch (true) {
      /*       case Object.values(DocumentExtension).includes(fileExtension as DocumentExtension):
        return Object.values(DocumentExtension);
      case Object.values(MediaExtension).includes(fileExtension as MediaExtension):
        return Object.values(MediaExtension); */
      case Object.values(ImageExtension).includes(fileExtension as ImageExtension):
        return Object.values(ImageExtension);
      default:
        return [];
    }
  });

  const handleFileUpload = (files: File[]) => files.length > 0 && (uploadedFile.value = files[0]);

  const convertFile = async () => {
    const formData = new FormData();
    uploadedFile.value && formData.append('file', uploadedFile.value);
    selectedExtension.value && formData.append('targetExtension', selectedExtension.value);

    const conversionPromise = (async () => {
      const response = await fetch(`${runtimeConfig.public.apiUrl}/convert`, {
        method: 'POST',
        body: formData
      });
      if (!response.ok) return response;
      const blob = await response.blob();
      convertedFile.value = blob;
      historyStore.addConversion({
        date: new Date().toISOString(),
        name: uploadedFile.value?.name || '',
        baseExtension: uploadedFile.value?.name.split('.').pop() || '',
        exportExtension: selectedExtension.value,
        blob,
        location: {
          lat: isSupported ? coords.value.latitude : null,
          lng: isSupported ? coords.value.longitude : null
        }
      });
    })();

    toast.promise(conversionPromise, {
      pending: 'Conversion en cours...',
      success: 'Conversion réussie !',
      error: {
        render({ data }) {
          return `Conversion échouée: ${data.message}`;
        }
      }
    });
  };

  return {
    convertedFile,
    convertFile,
    uploadedFile,
    selectedExtension,
    handleFileUpload,
    availableExtensions
  };
}
