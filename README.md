# FileConvertWizard

🌟 FileConvertWizard is a PWA (Progressive Web App) using Nuxt.js to offer a file conversion service. This application allows converting various file types (PDF, DOCX, TXT, JPEG, PNG, MP3, WAV) with advanced features such as internal storage, notifications, and more.

## About

📝 FileConvertWizard is developed with Nuxt.js for the front-end and a nodejs backend for handling file conversions. The application provides REST endpoints for managing conversions.

## Prerequisites

You'll need to set up a `.env` file with the necessary environment variables. Rename the `.env-sample` file to `.env` and fill in the required information, including database connection details.

## Installation

⚙️ Clone the GitHub repository:

```bash
git clone https://gitlab.com/front3491017/convertify.git
```

## Usage

📦 Install dependencies:

```bash
npm install
```

🚀 To start the application in development mode:

```bash
npm run dev
```

🌐 The application by default runs on port 8080.

## Information

## Contributing

🤝 As of now, there are no contributors to this project.

## Technologies Used

🛠️ - Nuxt (v3.12.1)
🚀 - Vue (v3.4.27)

## Roadmap

- [ ] Add more file types for conversion
- [ ] Add more advanced features
- [ ] Improve the UI/UX
- [ ] Add tests
- [ ] Add documentation
- [ ] Sync file compatible convert with the backend
