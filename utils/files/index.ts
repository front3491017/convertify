import { useShare } from '@vueuse/core';
import { toast } from 'vue3-toastify';

export const downloadFile = (blob: Blob) => {
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.href = url;
  a.download = 'converted';
  document.body.appendChild(a);
  a.click();
  a.remove();
};

export const shareFile = async (blob: Blob) => {
  const { isSupported, share } = useShare();
  if (!isSupported) return toast.error("L'API de partage Web n'est pas prise en charge sur ce navigateur.");
  try {
    await share({
      files: [
        new File([blob], 'converted', {
          type: blob.type
        })
      ]
    });
  } catch (error) {
    toast.error('Impossible de partager le fichier.');
  }
};

export const bytesToGigabytes = (bytes: number) => bytes / 1024 ** 3;
